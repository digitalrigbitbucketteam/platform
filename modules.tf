module "resource_group" {
  source = "./modules/resource_group"

  // pass variables from .tfvars
  owner         = "${var.owner}"
  name          = "${var.resource_group}"
  environment   = "${var.environment}"
  az_region     = "${var.az_region}"

  // inputs from other modules  
}

# module "api_gateway" {
#   source = "./modules/api_gateway"

#   // pass variables from .tfvars
#   owner         = "${var.owner}"
#   environment   = "${var.environment}"
#   az_region     = "${var.az_region}"

#   // inputs from other modules
#   resource_group_name = "${module.resource_group.resource_group_name}"
# }

