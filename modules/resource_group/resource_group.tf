resource "azurerm_resource_group" "rig" {
  name     = "${var.name}"
  location = "${var.az_region}"

  tags = {
    environment = "${var.environment}"
    owner       = "${var.owner}"
  }
}