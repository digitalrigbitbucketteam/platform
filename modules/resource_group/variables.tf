variable "owner" {
  type    = "string"
  description = "Owner of the resources"
}
variable "name" {
    type = "string"
    description = "Resource group name"
}
variable "environment" {
  type    = "string"
  description = "environment type"
}
variable "az_region" {
    type = "string"
    description = "Azure Region"
}