resource "azurerm_api_management" "rig" {
  name                = "rig-apim"
  location            = "${var.az_region}"
  resource_group_name = "${var.resource_group_name}"
  publisher_name      = "Amit"
  publisher_email     = "amit.sarkar5@wipro.com"

  sku {
    name     = "Developer"
    capacity = 1
  }
}