resource "azurerm_api_management_api" "rig" {
  name                = "conference-api"
  resource_group_name = "${var.resource_group_name}"
  api_management_name = "${azurerm_api_management.rg_test.name}"
  revision            = "1"
  display_name        = "Conference API"
  path                = "conference"
  protocols           = ["https"]

  import {
    content_format = "swagger-link-json"
    content_value  = "http://conferenceapi.azurewebsites.net/?format=json"
  }
}