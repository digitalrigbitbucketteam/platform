variable "owner" {
  default = "Amit"
  type    = "string"
  description = "Owner of the resources"
}
variable "environment" {
    type = "string"
    description = "Type of environment"
}
variable "az_region" {
    type = "string"
    description = "Azure Region"
}
variable "resource_group" {
    type = "string"
    description = "Resource group"
}
variable "subscription_id" {
    type = "string"
    description = "Subscription Id"
}
variable "client_id" {
    type = "string"
    description = "Client Id of the App"
}
variable "client_secret" {
    type = "string"
    description = "Client secret/password"
}
variable "tenant_id" {
    type = "string"
    description = "Tenant id"
}
